<!DOCTYPE html>
<html lang="en">
<head>
    <title>Php variables and functions</title>
</head>
<body>
    <h1>PHP variables</h1>
    <p>Variables are "containers" for storing information and they starts with the $ sign, followed by the name of the variable. Variables can store data of different types, and different data types can do different things.</p>
    <p>Some <b><i>data types</b></i> that PHP supports are:</p>

    <ul>

    <li><h3>String</h3> - is a sequence of characters, like "Hello world!".A string can be any text inside quotes. We can use single or double quotes. Here is an example:<br><br>
    <?php
    $color = "blue";
    $x = "Yooohoohoohooo";
    echo "This is really an eho string <span style='color:$color'>$x</span>";
    ?>
    </li>

    <li><h3>Integer</h3> - is a non-decimal number between -2,147,483,648 and 2,147,483,647. Rules for integers are that they must have at least one digit, not have a decimal point, can be either positive or negative and Integers can be specified in three formats: decimal (10-based), hexadecimal (16-based - prefixed with 0x) or octal (8-based - prefixed with 0). Here is an example:<br><br>
    <?php
    $color = "green";
    $x = 7;
    echo "This is the lucky integer <span style='color:$color'>" . $x . "</span>";
    var_dump($x);
    ?>
    </li>

    <li><h3>Float</h3> - (floating point number) is a number with a decimal point or a number in exponential form. In the following example $x is a float. The PHP var_dump() function returns the data type and value:<br><br>
    <?php
    $color = "orange";
    $x = 16.768;
    echo "This is float not the boat <span style='color:$color'>" . $x . "</span>";
    var_dump($x);
    ?>
    </li>

    <li><h3>Boolean</h3> - represents two possible states: TRUE or FALSE. Booleans are often used in conditional testing.<br><br>
    <?php
    $color = "pink";
    $x = true;
    echo "This is one side of the boolean <span style='color:$color'>" . $x . "</span><br>";
    $y = false;
    ?>
    </li>

    <li><h3>Array</h3> - stores multiple values in one single variable, and we can access the values by referring to an index number. Here is an example:<br><br>
    <?php
    $color = "red";
    $beer = array("Pivo", "Vopi", "Bez kragne molim!");
    echo "I like <span style='color:$color'>$beer[0]</span>, <span style='color:$color'>$beer[1]</span> and <span style='color:$color'>$beer[2]</span>";
    ?></li>

    </ul><br>

    <h2>Functions</h2>
    <?php 
    include 'function.php';
    sumcolor(array(2,4,8,16,32,64,128), "green");
    echo "<br>";
    sumcolor(array(4,9,18,64,98,156), "red");
    echo "<br>";
    sumcolor(array(2,4,8,16,32,64,128,256,512), "blue");
    ?>
    
</body>
</html>